-- phpMyAdmin SQL Dump
-- version 4.0.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 09, 2014 at 03:23 PM
-- Server version: 5.5.33
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `Forum_Coursework`
--

-- --------------------------------------------------------

--
-- Table structure for table `Answers`
--

CREATE TABLE `Answers` (
  `answerID` int(11) NOT NULL AUTO_INCREMENT,
  `questionID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `score` int(30) NOT NULL,
  `text` text NOT NULL,
  `timeCreated` date NOT NULL,
  `editorID` int(11) NOT NULL,
  `timeEdited` date NOT NULL,
  PRIMARY KEY (`answerID`),
  KEY `questionID` (`questionID`),
  KEY `editorID` (`editorID`),
  KEY `userID` (`userID`),
  KEY `editorID_2` (`editorID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `Answers`
--

INSERT INTO `Answers` (`answerID`, `questionID`, `userID`, `score`, `text`, `timeCreated`, `editorID`, `timeEdited`) VALUES
(1, 3, 1, 0, 'Yes games', '2013-12-05', 1, '2013-12-05'),
(9, 27, 7, 0, 'akjsdh', '2013-12-31', 7, '2013-12-31'),
(11, 4, 7, 0, 'ANSWER!', '2013-12-31', 7, '2013-12-31'),
(12, 4, 7, 0, 'Another answeR!', '2013-12-31', 7, '2013-12-31'),
(13, 5, 8, 0, 'heres an answer', '2014-01-06', 8, '2014-01-06');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('e5cdb745175c6326d0985f9ba9bc9877', '::1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:26.0) Gecko/20100101 Firefox/26.0', 1389276390, '');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `session_id` varchar(40) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`session_id`, `name`) VALUES
('0155f9a74ee6c7cd697ccd1ef3bec9da', 'Mod'),
('03c1d3b2ee03c18dc3ee3f7b0391ae32', 'Hassan'),
('03ce928f88dbf080f7868fff1572f80c', 'Hassan'),
('04e5638a8324ec11a2edb90310a2b09a', 'Mod'),
('081aef605d9079e49308e4c9566c9ec6', 'Hassan'),
('08e44ddb30627c834a1c92e4e73bf189', 'Mod'),
('0b9a1b9a6f23ddac41ac0f1a3b5d7eb5', 'Hassan'),
('0c038548f9aee81d8c51a88d6625f026', 'Mod'),
('0c9f956b6d0b590f4bf6c0f87894b81b', 'Hassan'),
('0d111375edccf0a357632b17717d3423', 'test'),
('0e193abe87cae63b48c88a123238a2d0', 'Hassan'),
('0e6f422f08847732b014bdce459012f4', 'Hassan'),
('10247cafa14ca5caf9eb7935f5adc934', 'Hassan'),
('14f6f692d636817f66ef81d260ac40dd', 'Hassan'),
('15c6a564bef5ea53a856d4c8061a180b', 'Mod'),
('1bf84e43f5505f4829d8d063775ccbfa', 'Hassan'),
('1c1dd7d604ca1111fdc4917f400e29de', 'Hassan'),
('1cd59f847985c846fd29177f6d4339bf', 'Hassan'),
('1cf780084c956e96226137ed038dd851', 'Hassan'),
('1f5f743bb7adc40b29a35f6cf7c08f78', 'Hassan'),
('210a6234092c3bc6d029829254393378', 'Hassan'),
('23378276e2a82a7747eda2d0b86cfd84', 'Hassan'),
('256edeb15bfbb67cea945646ab62c69a', 'Mod'),
('284cd49c705be0d1bb9773cdc360efe9', 'Hassan'),
('29f8cbbf05bd95f8018a5824fb5d8974', 'Hassan'),
('2a1a3b4918c5c4b05cbffefeac371a60', 'Mod'),
('2e1ce18c085f24f81f717aa6c3e3380d', 'Hassan'),
('2f3acd3470caaea216798cb2678a8ab7', 'Mod'),
('3139a4bc9c98babf99ccc20a35388b3e', 'Hassan'),
('317fb06bd9aa79aff51f99ce9056a41b', 'Hassan'),
('34ec59f572a3b37803c72ca00052c7ab', 'Mod'),
('356519b02ca06938cca061336c2f370b', 'Hassan'),
('420827c8a25b6120951e6175aaf68942', 'Hassan'),
('423773c4bb80147f551ae92d50590bff', 'Hassan'),
('42f6e9384f06b1b36338c89e046d8ff6', 'Hassan'),
('43933a66c7c9f154c0a976ceddd3a9e7', 'Hassan'),
('4686bcdfc1b01caa1b0d1fce21b334e3', 'Hassan'),
('468ab3e3dc84835cd41a29effb7b6142', 'Hassan'),
('4736cdd50b3b6966f6bdab75a04ee768', 'Hassan'),
('4974bcc5b733bde11cf384580e9c8349', 'Mod'),
('4ae365e49927fb66cd5a9939bea886b9', 'Hassan'),
('5089557e3dbc15b4e9486df42a35e536', 'Hassan'),
('511cff06852a42f9a488abd03e501b04', 'Mod'),
('5305e91d800c2260e5ca209a9156a4e9', 'Mod'),
('537f22b30ae74899e5844e66d038f4d7', 'Hassan'),
('57b1b8f19aa13628d8316f15feb8ab68', 'Hassan'),
('58bf020a597f1fc0d99757cd145343e6', 'Hassan'),
('5a8e667583b8b96f9bafcd093b1298e0', 'Hassan'),
('5dbbc2fcaa9f4ec1dcde33f2689213a9', 'Mod'),
('5ef7593d4dc934dab18af267bec91d84', 'Hassan'),
('5f47ffb6915357071ed7915eb6e2881b', 'Hassan'),
('6134fe7778df966cbb278643921118a7', 'Hassan'),
('6704b19f549c331f6d052edbf28dbb36', 'Mod'),
('67c44cae71e8d4a46db992da87e2eeb3', 'Mod'),
('6bb384e580041cd641834d3be91aef58', 'Hassan'),
('6eb5bf497d45254a2b8f6a21fb47644c', 'Hassan'),
('7154cc44e36b6b331f58757fca6f0817', 'Mod'),
('72265ce24a5955c9ac2012f0accf8f19', 'Hassan'),
('7234a5a62d86340cc92252a05460f292', 'Hassan'),
('73a58d6580a1e84b7b36a8f246582a3d', 'Hassan'),
('743bcf970fc9f7d29088f37332bcd8e5', 'Mod'),
('746b6bc7430abb4ab3444e821cda11fc', 'Hassan'),
('75ab51c01e17d175a7c0084e296dc9b8', 'Mod'),
('78fc1441d7d3b66c09aa3667808cf2e4', 'Mod'),
('79efe5a5fdfe14692d5a485cec7f7a9b', 'Hassan'),
('7b061fea7ff12ac850845ecb67613ec2', 'Hassan'),
('7b515d3ae021049f255908a5f70358a4', 'Hassan'),
('810b4561ec8c481b59cf6745cea802e7', 'admin'),
('815f8db4fbe95176dc2203266ff3fa00', 'Hassan'),
('81b8941e1194935937a6ddbd2ade4ce1', 'Mod'),
('896a820ec6bec12f63a91de9ad01a3ab', 'Hassan'),
('8c666582e82c227658ba591f70196841', 'Hassan'),
('8fbdc40c5c6e8f04e8167a43f494d6ca', 'Hassan'),
('979da4b13f81b6212ae9a13d20c09e23', 'Hassan'),
('97e0bf0ff1d1f505fa6d4d16d763dcc9', 'Mod'),
('9cc64cdf4a281faf5cb9b829650ea7b5', 'Hassan'),
('9e3d318321c219aea235ef989764ffa2', 'Hassan'),
('a0886ef6aa070818d16600ba129e0c95', 'Mod'),
('a50a48e64f9a0c0ee3abfb47540d87fb', 'Hassan'),
('a5bf292302a0119b7289650bd67c93cc', 'Hassan'),
('a626614cc53ee473586cb3124062b6a1', 'Hassan'),
('a78f0172688a15ce7656b068986e103e', 'Hassan'),
('a8b35e2e9c68ed06ed963e13d3502f74', 'Mod'),
('ad96b83d3e38a380da4bd0fe0b6be8e6', 'Hassan'),
('adc012ef22d3d08c242cf66e34049ae7', 'Hassan'),
('aed0360fedec9c03383913cc2e7e451c', 'Hassan'),
('b02693d7cc96106d1b52e667a3b9cbfe', 'Hassan'),
('b083bf423114d76f103585204ce43837', 'Hassan'),
('b1d6b93f84ee5aa44d82213c32f858ff', 'Hassan'),
('b443b729f9f3b6ddf38b5acdab9df8b9', 'Hassan'),
('b6c082d67a083d26d6b0b7a6040216b0', 'Hassan'),
('b82da2d511549040519a70bbd77abdf3', 'Hassan'),
('b85bba9dc96596de25342fdc29330a9a', 'Mod'),
('b8b8f4e22b11d2f6716dbcc02200d5eb', 'Mod'),
('bdb5a89414fa2215a850a0f10afdf2b3', 'Hassan'),
('bf7a5107b389da7865592b5e066593be', 'Hassan'),
('c169b483286e5bd4101d3111d79ff498', 'Hassan'),
('c52a15e44303fe1321460fdb225d7950', 'Hassan'),
('c55f741b042d44e22462f29c5d834d0e', 'banned'),
('c5a16b8746328718ad2d63a8121a447f', 'admin'),
('cd6501db471386163a4c74cb02a1993c', 'Mod'),
('cdbbb61f1a21ecaade5df9168e8d87aa', 'Hassan'),
('cf21a6f480886ea604bbaf40ce98b7e3', 'Hassan'),
('d07d4a5937797d6056f4ac57ec9f8f46', 'Hassan'),
('d0a5d1ba9893a554c782d1763b2fda79', 'Hassan'),
('d1acced2a93ee8b06cf76bf7ee0a2754', 'Hassan'),
('d498ff18c229126be471b1030d19ee6c', 'Hassan'),
('d765a76a431096cf8f250bbbd05b1531', 'Mod'),
('daf4186a61b6e1a455cc74aee3ab7671', 'Hassan'),
('db2f6b0eaa424db1859265b05b1232c4', 'Hassan'),
('dddb337b909bad24d250e43fe165c2a8', 'Hassan'),
('de5a4687a6fb2c14ff49b736f13c032b', 'Hassan'),
('de73cd8422ee86bc5c97828faaf57fe2', 'Hassan'),
('e21a7d00f2f46f6305c698b541afbe1a', 'Hassan'),
('e5d7687654620b353a53e40c71b1eddd', 'Hassan'),
('e63c496de1cf732fffc911c4791cde51', 'Mod'),
('e6a9a51c47c2bcaf887e542c4c2662cc', 'Hassan'),
('e788793549b6c7a5bc456fab619c3f22', 'Hassan'),
('e796c6b60ba9ae40f5ab1b8f68be8b39', 'Hassan'),
('e8184eb7e39f0363babc1a05017f7d93', 'Hassan'),
('ea26fce20fed64d479e5629fca14fdf4', 'Hassan'),
('ef3407306d044e1967eb2f4d82ecbfa8', 'banned'),
('f19f9198a00030e3044461eda86f3870', 'Hassan'),
('f47c7e19fd0b7614f7a8bc1f0001fcf1', 'Mod'),
('f6f2fa9f9fd28a58650b78032e0ad7c9', 'Hassan'),
('f8d19c3c41032482920bc3612e9a465b', 'Mod'),
('f9778222b49d6da49539c158ba6fb8a8', 'Hassan'),
('fa4a52ac898b452d71b4fa1248747126', 'Hassan'),
('fb45b468a44a9e840df13a9630c830e5', 'Hassan'),
('fb83a15ba1199b3d42d87f8f7733e7cf', 'test'),
('fdd9db6c4f1d852e26404e924fd9448c', 'Hassan');

-- --------------------------------------------------------

--
-- Table structure for table `Questions`
--

CREATE TABLE `Questions` (
  `questionID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `text` text NOT NULL,
  `score` int(20) NOT NULL,
  `userID` int(11) NOT NULL,
  `timeCreated` date NOT NULL,
  `editorID` int(11) NOT NULL,
  `timeEdited` date NOT NULL,
  `is_closed` tinyint(1) NOT NULL,
  PRIMARY KEY (`questionID`),
  KEY `userID` (`userID`),
  KEY `editorID` (`editorID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `Questions`
--

INSERT INTO `Questions` (`questionID`, `title`, `text`, `score`, `userID`, `timeCreated`, `editorID`, `timeEdited`, `is_closed`) VALUES
(3, 'Games', 'what games?', 10, 1, '2013-12-05', 1, '2013-12-05', 0),
(4, 'More about games', 'yay gamesss', 0, 7, '2013-12-19', 8, '2014-01-08', 0),
(5, 'Whats life without games', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nec lobortis erat. Suspendisse sagittis sodales dolor eget mattis. Ut pharetra arcu dictum risus imperdiet, et ultricies sapien semper. Etiam rutrum sit amet arcu quis ornare. Integer mattis est sapien, quis elementum nibh tempus in. Vivamus ac dui eu odio molestie convallis vitae quis lorem. Pellentesque ornare lorem nec lectus ultricies, vitae interdum est malesuada. Duis facilisis sed purus ac venenatis. Ut feugiat rutrum nibh ac placerat. Mauris iaculis sapien massa, eget condimentum nisi ornare eget. ', 4, 7, '2013-12-24', 8, '2014-01-08', 0),
(6, 'Playstation', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nec lobortis erat. Suspendisse sagittis sodales dolor eget mattis. Ut pharetra arcu dictum risus imperdiet, et ultricies sapien semper. Etiam rutrum sit amet arcu quis ornare. Integer mattis est sapien, quis elementum nibh tempus in. Vivamus ac dui eu odio molestie convallis vitae quis lorem. Pellentesque ornare lorem nec lectus ultricies, vitae interdum est malesuada. Duis facilisis sed purus ac venenatis. Ut feugiat rutrum nibh ac placerat. Mauris iaculis sapien massa, eget condimentum nisi ornare eget. ', 0, 7, '2013-12-24', 8, '2014-01-08', 0),
(15, 'im asking a question', 'is it?', 1, 7, '2013-12-30', 7, '2013-12-30', 1),
(27, 'Update', 'update', 0, 7, '2013-12-30', 7, '2014-01-01', 0),
(42, 'Update v2', 'updateedededededed', 0, 7, '2013-12-31', 7, '2013-12-31', 0),
(43, 'asd', 'sdasd', 0, 7, '2013-12-31', 7, '2013-12-31', 0),
(46, 'woo another update', 'yeahhhhhh', 1, 7, '2013-12-31', 7, '2014-01-01', 0),
(47, 'asd', 'asd', 0, 7, '2013-12-31', 7, '2013-12-31', 1),
(48, 'aklsdhj', 'kjashd', 0, 7, '2013-12-31', 7, '2013-12-31', 0),
(49, 'Has this worked?', 'Please?', 0, 7, '2013-12-31', 7, '2013-12-31', 1),
(50, 'Tes', 'test', 0, 7, '2013-12-31', 7, '2013-12-31', 0),
(51, 'Test', 'tester', 0, 7, '2013-12-31', 7, '2013-12-31', 0),
(52, 'test', 'test', 0, 7, '2013-12-31', 7, '2013-12-31', 0),
(53, 'Im testing stuff', 'Really? Yeah really', 0, 7, '2013-12-31', 8, '2014-01-09', 0),
(54, 'PHP', 'Heres some php simon', 0, 7, '2014-01-01', 7, '2014-01-01', 0),
(55, 'Javascript', 'javascript for simon', 0, 7, '2014-01-01', 7, '2014-01-01', 0),
(56, 'codeigniter', 'this is codeigniter', 0, 7, '2014-01-01', 7, '2014-01-01', 0),
(58, 'Javascript PHP', 'asd', 0, 7, '2014-01-01', 7, '2014-01-01', 0),
(59, 'test question', 'tester', 0, 12, '2014-01-08', 12, '2014-01-08', 0);

-- --------------------------------------------------------

--
-- Table structure for table `QuestionTags`
--

CREATE TABLE `QuestionTags` (
  `questionID` int(11) NOT NULL,
  `tagID` int(11) NOT NULL,
  KEY `questionID` (`questionID`),
  KEY `tagID_2` (`tagID`),
  KEY `tagID_3` (`tagID`),
  KEY `tagID_4` (`tagID`),
  KEY `tagID_5` (`tagID`),
  KEY `questionID_2` (`questionID`),
  KEY `tagID_6` (`tagID`),
  KEY `tagID_7` (`tagID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `QuestionTags`
--

INSERT INTO `QuestionTags` (`questionID`, `tagID`) VALUES
(4, 2),
(4, 6),
(5, 11),
(6, 12),
(3, 13),
(4, 11),
(27, 11),
(27, 8),
(42, 1),
(42, 2),
(42, 3),
(42, 4),
(42, 5),
(42, 6),
(42, 7),
(42, 8),
(42, 9),
(42, 10),
(42, 11),
(42, 12),
(42, 13),
(43, 1),
(43, 2),
(43, 3),
(43, 4),
(43, 5),
(43, 6),
(43, 7),
(43, 8),
(43, 9),
(43, 10),
(43, 11),
(43, 12),
(43, 13),
(48, 1),
(48, 4),
(49, 1),
(49, 2),
(49, 3),
(49, 12),
(49, 13),
(50, 1),
(50, 3),
(51, 2),
(51, 3),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 2),
(55, 2),
(56, 3),
(15, 5),
(59, 2),
(59, 3);

-- --------------------------------------------------------

--
-- Table structure for table `Tags`
--

CREATE TABLE `Tags` (
  `tagID` int(11) NOT NULL,
  `tags` varchar(100) NOT NULL,
  PRIMARY KEY (`tagID`),
  UNIQUE KEY `tagID_3` (`tagID`),
  KEY `tagID` (`tagID`),
  KEY `tagID_2` (`tagID`),
  KEY `tagID_4` (`tagID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Tags`
--

INSERT INTO `Tags` (`tagID`, `tags`) VALUES
(1, 'Playstation'),
(2, 'XBOX'),
(3, 'Nintendo'),
(4, 'FPS'),
(5, 'Sports'),
(6, 'RPG'),
(7, 'Puzzle'),
(8, 'MMO'),
(9, 'Fighting'),
(10, 'Fantasy'),
(11, 'Driving'),
(12, 'Sandbox'),
(13, 'Indie');

-- --------------------------------------------------------

--
-- Table structure for table `UserProfiles`
--

CREATE TABLE `UserProfiles` (
  `userID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `reputation` int(50) NOT NULL,
  `role` varchar(10) NOT NULL,
  KEY `userID` (`userID`),
  KEY `userID_2` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserProfiles`
--

INSERT INTO `UserProfiles` (`userID`, `name`, `reputation`, `role`) VALUES
(1, 'Tasneem', 10, '3'),
(7, 'Hassan', 4, '2'),
(8, 'Mod', 0, '2'),
(10, 'banned', 0, '1'),
(11, 'admin', 0, '3'),
(12, 'test', 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `isBanned` tinyint(1) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`userID`, `username`, `password`, `isBanned`) VALUES
(1, 'Tasneem', 'Rahim', 0),
(7, 'Hassan', '3c8ec4874488f6090a157b014ce3397ca8e06d4f', 0),
(8, 'Mod', '7dd30f0a95d522bfc058be4e75847f8b6df9f76b', 0),
(10, 'banned', '2580d49ff4060824fc921008b52e8e6cd9380570', 1),
(11, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 0),
(12, 'test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 0),
(13, 'jad', 'jad', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Answers`
--
ALTER TABLE `Answers`
  ADD CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`questionID`) REFERENCES `Questions` (`questionID`),
  ADD CONSTRAINT `answers_ibfk_2` FOREIGN KEY (`userID`) REFERENCES `UserProfiles` (`userID`),
  ADD CONSTRAINT `answers_ibfk_3` FOREIGN KEY (`editorID`) REFERENCES `UserProfiles` (`userID`);

--
-- Constraints for table `Questions`
--
ALTER TABLE `Questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `UserProfiles` (`userID`),
  ADD CONSTRAINT `questions_ibfk_2` FOREIGN KEY (`editorID`) REFERENCES `UserProfiles` (`userID`);

--
-- Constraints for table `QuestionTags`
--
ALTER TABLE `QuestionTags`
  ADD CONSTRAINT `questiontags_ibfk_1` FOREIGN KEY (`questionID`) REFERENCES `Questions` (`questionID`),
  ADD CONSTRAINT `questiontags_ibfk_2` FOREIGN KEY (`tagID`) REFERENCES `Tags` (`tagID`);

--
-- Constraints for table `UserProfiles`
--
ALTER TABLE `UserProfiles`
  ADD CONSTRAINT `userprofiles_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`);
