<?php

class editModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->model('adminModel');
    }

    public function loadEdit($id) {
        //loads the edit page if it was created by the same user or the user is 
        //one with priviledges
        $username = $this->authlib->is_loggedin();
        $sameUser = $this->getUser($id);
        $role = $this->adminModel->getRole($username);
        if ($sameUser === $username || $role > 1) {
            $this->db->select('*')->from('Questions')->where('questionID', $id);
            $query = $this->db->get();
            //get the question information and return to controller
            return($query);
        } else {
            redirect('/mainController/questions/' . $id);
        }
    }

    public function getUser($id) {
        //get the user name from the UserProfiles with the user ID in the 
        //questions table by the question id
        $query = $this->db->get_where('Questions', array('questionID' => $id));
        $result = $query->result();
        foreach ($result as $row) {
            $uID = $row->userID;
            break;
        }
        $q = $this->db->get_where('UserProfiles', array('userID' => $uID));
        $uResult = $q->result();
        foreach ($uResult as $uRow) {
            $name = $uRow->name;
            break;
        }
        return $name;
    }

    public function edit($id, $text, $title) {
        //function which submits the edit
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            redirect('/mainController');
        } else {
            //once the user is logged in it gets the users id again
            $query = $this->db->get_where('UserProfiles', array('name' => $username));
            $result = $query->result();
            foreach ($result as $row) {
                $uID = $row->userID;
                break;
            }
        }
        //lastly inserts everything in to the questions table with the given question id
        $date = date('Y-m-d');
        $data = array('title' => $title, 'text' => $text, 'editorID' => $uID, 'timeEdited' => $date);
        $this->db->where('questionID', $id)->update('Questions', $data);
    }

}

/* 
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

