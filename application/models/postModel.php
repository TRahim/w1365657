<?php

class postModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('user');
    }

    public function post($title, $text, $username, $tags) {
        //checks if the user is logged in
        if ($username === false) {
            redirect('/mainController');
        } else {
            $data = $this->db->get_where('UserProfiles', array('name' => $username));
            //gets the user profiles by the username
            $result = $data->result();
            foreach ($result as $row) {
                $uID = $row->userID;
                break;
            }
            //gets user id from the query
        }
        //get the date using php built in function
        $date = date('Y-m-d');
        //insert into questions with all the user sent data and the variables we just got
        $this->db->insert('Questions', array('title' => $title, 'text' => $text,
            'score' => 0, 'userID' => $uID, 'timeCreated' => $date, 'editorID' => $uID, 'timeEdited' => $date, 'is_closed' => 0));
        $id = $this->db->insert_id();
        //get the last id in the database
        foreach ($tags as $value) {
            //for every tag sent search in the tags table
            $this->db->select('*')->from('tags')->where_in('tags', $value);
            $query = $this->db->get();
            $result = $query->result();
            foreach ($result as $row) {
                //get the tag id of each tag and insert into the questions tags table with the question id we gathered eariler
                $tID = $row->tagID;
                $this->db->insert('QuestionTags', array('questionID' => $id, 'tagID' => $tID));
            }
        }
        return $id;//return the id
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

