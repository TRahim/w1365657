<?php

class answerModel extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    public function answerQ($id, $text){
        //checks if the suer is logged in
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            redirect('/mainController');
        } else {
            //searches in the userprofiles tables to get the userid
            $data = $this->db->get_where('UserProfiles', array('name' => $username));
            $result = $data->result();
            foreach ($result as $row) {
                $uID = $row->userID;
                break;
            }
        }
        //add to the answers table with the text and ids gathered, and also with a score
        $date = date('Y-m-d');
        $this->db->insert('Answers', array('questionId' => $id, 'text' => $text, 'userID' => $uID, 'editorID' => $uID, 'timeCreated' => $date, 'timeEdited' => $date, 'score' => 0));
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

