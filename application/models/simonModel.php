<?php

class simonModel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function search($value) {
        // as we get a multidimensional we need two loops
        //first one to get into the array we want and then in the second one
        //we search by each word and get the values and return it
        foreach($value as $row){
            foreach($row as $title){
                $this->db->like('title', $title);
            }
        }
        $data = $this->db->get('Questions');
        return $data;
    }
    
    public function advancedSearch($question, $tags,$user) {
        $this->db->select('*');
        $this->db->from('Questions');

        if ($question !== "") {
            foreach ($question as $row) {
                foreach ($row as $q) {
                    $this->db->like(array('title' => $q));
                }
            }
        }

        if (!(empty($tags))) {
            $arrCount = sizeof($tags);
            $this->db->join('QuestionTags', 'QuestionTags.questionID = Questions.questionID'); //join qID and tID 
            $this->db->join('Tags', 'Tags.tagID = QuestionTags.tagID');
            $this->db->where_in('tags', $tags);
            $this->db->having('COUNT(DISTINCT tags )=', $arrCount);
        }

        if ($user !== "") {
            $this->db->join('UserProfiles', 'UserProfiles.userID = Questions.userID');
            $this->db->where(array('name' => $user));
        }
        $this->db->group_by("Questions.questionID");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
//    public function getTags($data){
//        $qID = array();
//        foreach($data as $row){
//            $qID[] = $row->questionID;
//        }
//        $this->db->select('tags');
//        $this->db->from('Tags');
//        $this->db->join('QuestionTags', 'QuestionTags.tagID = Tags.tagID');
//        $this->db->join('Questions', 'Questions.questionID = Questions.questionID');
//        $this->db->where_in('QuestionTags.questionID',$qID);
//        $this->db->group_by('QuestionTags.questionID');
//        $query = $this->db->get();
//        $result = $query->result();
//        return $result;
//    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

