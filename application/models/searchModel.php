<?php

class searchModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function search($value) {
        //same as the search in simons model
        foreach ($value as $row) {
            foreach ($row as $title) {
                $this->db->like('title', $title);
            }
        }
        $data = $this->db->get('Questions');
        return $data;
    }

    public function isClosed($id) {
        //checks if the question is closed
        $data = $this->db->get_where('Questions', array('is_closed' => 1, 'questionID' => $id));
        //searches if the question with the given id is closed
        $rows = $data->num_rows();
        $closed = 1;
        $open = 0;
        //if the question is closed then there would be a row matching
        if ($rows !== 0) {
            return $closed;
            //send value of 1 if closed
        } else {
            return $open;
            //send value of 0 if open
        }
    }

    public function getTitle($id) {
        //get the question title from the id
        $data = $this->db->get_where('Questions', array('questionID' => $id));
        return($data);
    }

    public function getUser($id) {
        //function for getting the user
        //search for the userID in the question table
        $data = $this->db->get_where('Questions', array('questionID' => $id));
        $result = $data->result();
        foreach ($result as $row) {
            $user = $row->userID;
            break;
        }
        //return the user data from 
        $data = $this->db->get_where('UserProfiles', array('userID' => $user));
        return($data);
    }

    public function loadTags() {
        //get all data from the tags table
        $data = $this->db->get('Tags');
        return($data);
    }

    public function getTags($id) {
        //get all tags in the question id
        //join two tables together here and search
        $this->db->select('*');
        $this->db->from('Tags');
        $this->db->join('QuestionTags', 'QuestionTags.tagID = Tags.tagID');
        $this->db->where('QuestionTags.questionID', $id);
        $result = $this->db->get();

        return($result);
    }

    public function getTagID($id) {
        //get the tagID depending on what the tag is
        $data = $this->db->get_where('Tags', array('tags' => $id));
        $result = $data->result();
        foreach ($result as $row) {
            $tagID = $row->tagID;
            break;
        }
        if ($tagID === null) {
            return "";
        }
        return $tagID;
    }

    public function searchTag($id) {
        //search by tags in this function

        $arrCount = sizeof($id);
        $this->db->select('*');
        $this->db->from('Questions');
        $this->db->join('QuestionTags', 'QuestionTags.questionID = Questions.questionID'); //join qID and tID 
        $this->db->join('Tags', 'Tags.tagID = QuestionTags.tagID');
        $this->db->where_in('tags', $id);
        $this->db->group_by("Questions.questionID");
        $this->db->having('COUNT(DISTINCT tags )=', $arrCount);

        $result = $this->db->get();

        return($result);
    }

    public function getAnswers($id) {
        //get all answers for a certain question in this function
        $data = $this->db->get_where('Answers', array('questionID' => $id));
        return $data;
    }

    public function addRep($id) {
        //add reputation to the user in this function, when their question gets voted up
        $data = $this->db->get_where('UserProfiles', array('userID' => $id));
        $result = $data->result();
        foreach ($result as $row) {
            $reputation = $row->reputation;
        }
        //get the users reputation and then adds 1 to it
        $reputation++;
        $arr = array('reputation' => $reputation);
        $this->db->where('userID', $id)->update('UserProfiles', $arr);
    }

    public function reduceRep($id) {
        //exactly the same as addRep but the opposite as now their rating goes down
        $data = $this->db->get_where('UserProfiles', array('userID' => $id));
        $result = $data->result();
        foreach ($result as $row) {
            $reputation = $row->reputation;
        }
        $reputation--;
        $arr = array('reputation' => $reputation);
        $this->db->where('userID', $id)->update('UserProfiles', $arr);
    }

    public function voteUp($id) {
        //function for when the user votes, it gets the question score from the id
        //and then off of the score we add to the score and then call the addRep
        //function above 
        $data = $this->db->get_where('Questions', array('questionID' => $id));
        $result = $data->result();
        foreach ($result as $row) {
            $score = $row->score;
            $userID = $row->userID;
        }
        $score++;
        $arr = array('score' => $score);
        $this->db->where('questionID', $id)->update('Questions', $arr);
        $this->addRep($userID);
    }

    public function voteDown($id) {
        //same as voteup pretty much but instead we minus
        $data = $this->db->get_where('Questions', array('questionID' => $id));
        $result = $data->result();
        foreach ($result as $row) {
            $score = $row->score;
            $userID = $row->userID;
        }
        $score--;
        $arr = array('score' => $score);
        $this->db->where('questionID', $id)->update('Questions', $arr);
        $this->reduceRep($userID);
    }

    public function getRecommendations($id) {
        $data = $this->getTags($id);
        $result = $data->result();
        $tags = array();
        foreach ($result as $row) {
            $tags[] = $row->tags;
        }
        $this->db->select('*');
        $this->db->from('Questions');
        $this->db->join('QuestionTags', 'QuestionTags.questionID = Questions.questionID');
        $this->db->join('Tags', 'Tags.tagID = QuestionTags.tagID');
        $this->db->where_in('tags', $tags);
        $this->db->group_by("Questions.questionID");
        $query = $this->db->get();
        return($query);
    }

    public function advancedSearch($question, $tags, $user) {
        $this->db->select('*');
        $this->db->from('Questions');
        //searhces in questions
        if ($question !== "") {
            foreach ($question as $row) {
                foreach ($row as $q) {
                    //checks for each value in array like in the title
                    $this->db->like(array('title' => $q));
                }
            }
        }
        
        //checks if the tags array is not empty
        if (!(empty($tags))) {
            $arrCount = sizeof($tags);
            $this->db->join('QuestionTags', 'QuestionTags.questionID = Questions.questionID'); //join qID and tID 
            $this->db->join('Tags', 'Tags.tagID = QuestionTags.tagID');
            $this->db->where_in('tags', $tags);
            $this->db->having('COUNT(DISTINCT tags )=', $arrCount);//make sure that the number of tags in the result are the same as the number of tags in the array
        }

        if ($user !== "") {
            $this->db->join('UserProfiles', 'UserProfiles.userID = Questions.userID');
            $this->db->where(array('name' => $user));
        }
        $this->db->group_by("Questions.questionID");//group them by question id to stop repeated results
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function userSearch($user) {
        $idQuery = $this->db->get_where('UserProfiles', array('name' => $user));
        $idResult = $idQuery->result();
        foreach ($idResult as $row) {
            $uID = $row->userID;
        }
        $qQuery = $this->db->get_where('Questions', array('userID' => $uID));
        $result = $qQuery->result();
        return ($result);
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

