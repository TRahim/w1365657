<?php

class adminModel extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    public function getRole($username){
        //works out what role the user has from their username
        $query = $this->db->get_where('UserProfiles', array('name' => $username));
        $result = $query->result();
        foreach($result as $row){
            $role = $row->role;
        }
        return $role;
    }
    public function close($id){
        //function to close a question, just simply changes the is_closed column
        $data = array('is_closed' => 1);
        $this->db->where('questionID', $id)->update('Questions', $data);
    }
    
    public function ban($uName){
        //function to ban a user, the same way as the close question function
        $data = array('isBanned' => 1);
        $this->db->where('Username', $uName)->update('Users', $data);
    }
    
    public function addMod($username){
        //addmod function similar to the last two changes a value in the column
        $data = array('role' => 2);
        $this->db->where('Name', $username)->update('UserProfiles', $data);
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

