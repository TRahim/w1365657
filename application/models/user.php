<?php

class user extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    function register($name, $username, $pwd) {
        // is username unique?
        $res = $this->db->get_where('users', array('username' => $username));
        if ($res->num_rows() > 0) {
            return 'Username already exists';
        }
        // username is unique
        $hashpwd = sha1($pwd);
        $data = array('username' => $username, 'password' => $hashpwd, 'isBanned' => 0);
        $this->db->insert('Users', $data);
        $id = $this->db->insert_id();
        $profiles = array('userID' => $id, 'name' => $username, 'reputation' => 0, 'role' => 1);
        $this->db->insert('UserProfiles', $profiles);
        return null; // no error message because all is ok
    }

    function login($username, $pwd) {
        $this->db->where(array('username' => $username, 'password' => sha1($pwd)));
        $res = $this->db->get('Users', array('username'));
        if ($res->num_rows() != 1) { // should be only ONE matching row!!
            return false;
        }
        $this->db->where(array('isBanned' => 1, 'username' => $username));
        $banned = $this->db->get('Users');
        if ($banned->num_rows() === 1) {
            return false;
        }
        // remember login
        $session_id = $this->session->userdata('session_id');
        // remember current login
        $row = $res->row_array();
        $this->db->insert('logins', array('name' => $row['username'], 'session_id' => $session_id));
        return $res->row_array();
    }

    function check_and_change($data) {
        //Check that the new and old passwords aren't the same.
        if ($data['oldpassword'] == $data['newpassword']) {
            return "Old and new passwords are the same.";
        }
        //Check if all fields have been filled in.
        if (($data['username'] == "") || ($data['oldpassword'] == "") || ($data['newpassword'] == "") || ($data['confirmpassword'] == "")) {
            return "Field value missing.";
        }
        //Check if confirmed password and new password don't match
        if ($data['confirmpassword'] != $data['newpassword']) {
            return "New password and confirmed password don't match.";
        }

        //Update the password for the relevant username (hence the where clause first)
        //(should really pass in an array of values. Key corresponds to column name.
        $this->db->where('username', $data['username']);
        $this->db->update('users', array('password' => sha1($data['newpassword']))); //Come back here.
        return null;
    }

    function is_loggedin() {
        $session_id = $this->session->userdata('session_id');
        $res = $this->db->get_where('logins', array('session_id' => $session_id));
        if ($res->num_rows() == 1) {
            $row = $res->row_array();
            return $row['name'];
        } else {
            return false;
        }
    }

    function getRole($id) {
        $data = $this->db->get_where('UserProfiles', array('name' => $id));
        $result = $data->result();
        foreach ($result as $row) {
            $role = $row->role;
        }
        return $role;
    }

    function getRep($id) {
        $data = $this->db->get_where('UserProfiles', array('name' => $id));
        $result = $data->result();
        foreach ($result as $row) {
            $reputation = $row->reputation;
        }
        return $reputation;
    }

}
