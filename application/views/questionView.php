<html>
    <body>
        <h1 style="text-align: center">Question pages</h1>
        <table style="float: left" border="1">
            <tr>
                <th><a href="/w1365657/index.php/mainController">Home</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/category">Categories</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/searchPage">Search for a question</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/askPage">Ask a question</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/contact">Contact</a></th>
            </tr>
        </table>
        <div style="border-left-width: 400px; padding-left: 160px;">
            <h4 style="text-align: left">

                <?php
                foreach ($results as $row) {
                    echo $row->title;
                }
                ?>
            </h4>
            <?php
            foreach ($results as $row) {
                $id = $row->questionID;
                echo '<a href="' . site_url('mainController/editView/' . $id) . '">';
                echo "Edit this question";
                echo "</a>";
            }
            ?>
            <br>
            <br>
            <div id="text">
                <?php
                foreach ($results as $row) {
                    echo $row->text;
                }
                ?>
                <div id="rating">
                    <?php
                    echo "Question rating: ";
                    foreach ($results as $row) {
                        echo $row->score;
                    }
                    ?>
                </div>
            </div>
            <?php
            foreach ($userResult as $row) {
                $id = $row->name;
                echo '<a href="' . site_url('mainController/userProfile/' . $id) . '">';
                echo $row->name;
                echo "</a>";
            }
            echo "<br/>";
            echo "Tags: ";
            foreach ($tags as $row) {
                echo $row->tags;
                echo ", ";
            }
            echo "<br>";
            echo "<br>";
            foreach ($answers as $row) {
                echo "Answer: ";
                echo "<br>";
                echo $row->text; 
                echo "<br>";
                echo "  Score: " . $row->score;
                echo "<br>";
            }
            foreach ($results as $row) {
                $id = $row->questionID;
                echo '<a href="' . site_url('mainController/voteUp/' . $id) . '">';
                echo "Vote Up";
                echo "</a>";
                echo "     ";
                echo '<a href="' . site_url('mainController/voteDown/' . $id) . '">';
                echo "Vote Down";
                echo "</a>";
            }
            ?>
            <br>
            <?php
            foreach ($results as $row) {
                $id = $row->questionID;
                echo '<a href="' . site_url('mainController/answerView/' . $id) . '">';
                echo "Answer This Question";
                echo "</a>";
            }
            echo "<br>";
            if ($userRole > 1) {
                echo '<a href="' . site_url('mainController/closeQuestion/' . $id) . '">';
                echo "Close question";
                echo "</a>";
            }
            echo "<br>";
            echo "<br>";
            echo '<h5> Here are some similar questions </h5>';
            foreach($recommended as $row){
                $id = $row->questionID;
                $title = $row->title;
                echo '<a href="' . site_url('mainController/questions/' . $id) . '">';
                echo $title;
                echo "</a>";
                echo "<br>";
            }
            ?>
        </div>
    </body>
</html>