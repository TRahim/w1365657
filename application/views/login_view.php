<?php
if ($user !== false) {
    redirect("/mainController");
}
?>
<html>
    <head>
        <title>Login/Register</title>
    </head>
    <body>
        <h1 style="text-align:center">Login/Register</h1>
        <table style="float: left" border="1">
            <tr>
                <th><a href="/w1365657/index.php/mainController">Home</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/category">Categories</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/searchPage">Search for a question</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/askPage">Ask a question</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/contact">Contact</a></th>
            </tr>
        </table>
        <form action="/w1365657/index.php/auth/authenticate" method="POST">
            Username : <input type="text" name='uname' length="10" size="10" />  <br>
            Password: <input type="password" name='pword' length="15" size="30" /> <br>
            <input type="submit" value='Login'>
        </form>
        <div name="register" style="text-align: right">
            <form action="/w1365657/index.php/auth/createaccount" method="POST">
                Enter your name (max 50 characters):
                <input type="text" name='name' length="20" size="50">  <br>
                Choose your username (max 10 characters):
                <input type="text" name='uname' length="10" size="10">  <br>
                Choose password:
                <input type="password" name='pword' length="15" size="30"> <br>
                Confirm password:
                <input type="password" name='conf_pword' length="15" size="30">
                <input type="submit" value='Register'>
            </form>
            <span style="color: red">
                <?php
                if (isset($errmsg)) {
                    echo $errmsg;
                }
                ?>
            </span> 
            <br>
        </div>
    </body>
</html>
