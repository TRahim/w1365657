<html>
    <body>
        <h1>Post a question</h1>
        <table style="float: left" border="1">
            <tr>
                <th><a href="/w1365657/index.php/mainController">Home</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/category">Categories</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/searchPage">Search for a question</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/askPage">Ask a question</a></th>
            </tr>
            <tr>
                <th><a href="/w1365657/index.php/mainController/contact">Contact</a></th>
            </tr>
        </table>
        <form action="/w1365657/index.php/mainController/postQuestion" method="post">
            <input type="text" maxlength="30" placeholder="Enter Title here" name="title" style="text-align: center" size="40">
            <br>
            <br>
            <textarea placeholder="Enter text here" name="text" cols="40" rows="10"></textarea>
            <br>
            Use ctrl or command to select more than one tags
            <select name="tags[]" multiple="">
                <?php
                foreach($tags as $row){
                    $tag = $row->tags;
                    echo $tag;
                    echo "<option value=$tag>$tag</option>";
                }
                ?>
            </select>
            <br>
            <input type="submit" value="Ask Question">
        </form>
    </body>
</html>

