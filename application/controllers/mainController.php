<?php

class mainController extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //Loading all my models and libraries here
        $this->load->model('searchModel');
        $this->load->model('postModel');
        $this->load->model('categoryModel');
        $this->load->library('authlib');
        $this->load->model('answerModel');
        $this->load->model('editModel');
        $this->load->model('adminModel');
        $this->load->model('user');
    }

    public function index() {
        //in the index I check if the user is logged in
        //then I load the homePage view
        $loggedin['user'] = $this->authlib->is_loggedin();
        $this->load->view('homePage', $loggedin);
    }

    public function category() {
        //category function used to just load all tags
        $data = $this->categoryModel->load(); //call my model to do the searching 
        $result = $data->result(); //return data and load the view with the results
        $this->load->view('categoryView', array('results' => $result));
    }

    public function searchPage() {
        //load the search question view
        $this->load->view('searchView');
    }

    public function askPage() {
        //load the ask question view, and make sure the user is logged in
        $loggedin = $this->authlib->is_loggedin();
        if ($loggedin === false) {
            $this->load->helper('url');
            redirect('/auth');
        }
        //load the tags from the search model and then pop all tags into the view for the drop down menu
        $tags = $this->searchModel->loadTags();
        $result = $tags->result();
        $this->load->view('postView', array('tags' => $result));
    }

    public function contact() {
        //load spoof contact view
        $this->load->view('contactView');
    }

    public function searchQuestion() {
        //get the form entry
        $question = $this->input->get('question');
        //trim and explode the field to try and get every word entered
        $arr = array_map('trim', explode(" ", $question));
        $array = array();
        foreach ($arr as $row) {
            if ($row !== "") {
                $array[] = array('title' => $row);
            }
        }
        //clear out white space with the foreach statement so only the words are searched for
        $details = $this->searchModel->search($array); //call the model and return data to be outputted in the view
        $result = $details->result();
        $this->load->view('searchView', array('questions' => $result));
    }

    public function postQuestion() {
        //post question function here
        $title = $this->input->post('title'); //gets values from the form
        $text = $this->input->post('text');
        $tags['tags'] = $this->input->post('tags'); //gets array of tags from the drop down box
        $username = $this->authlib->is_loggedin(); //again check if the user is logged in
        if ($username === false) {
            redirect('/mainController');
        }
        $data = $this->postModel->post($title, $text, $username, $tags); //call my model and send all data that I have gathered
        redirect('/mainController/questions/' . $data); //load the new question
    }

    public function questions($id) {
        //get all information about the question by using the id
        $title = $this->searchModel->getTitle($id);
        $user = $this->searchModel->getUser($id);
        $tags = $this->searchModel->getTags($id);
        $answer = $this->searchModel->getAnswers($id);
        $recommend = $this->searchModel->getRecommendations($id);

        $answerResult = $answer->result();
        $tagsResult = $tags->result();
        $userResult = $user->result();
        $result = $title->result();
        $recoResult = $recommend->result();
        //print_r($recoResult);
        //check if the user is logged in and if they are get what role they are, if they dont have a role just give them a non admin role
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            $role = 1;
        } else {
            $role = $this->adminModel->getRole($username);
        }
        $this->load->view('questionView', array('results' => $result, 'userResult' => $userResult, 'tags' => $tagsResult, 'answers' => $answerResult, 'userRole' => $role, 'recommended' => $recoResult));
    }

    public function searchTags() {
        //get the tag input value and search in the model again
        $question = $this->input->get('tags');
        $arr = array_map('trim', explode(" ", $question));
        $array = array();
        foreach ($arr as $row) {
            if ($row !== "") {
                $array[] = $row;
            }
        }
        //print_r($array);
        $detail = $this->searchModel->searchTag($array);
        $result = $detail->result();
        $this->load->view('searchView', array('tags' => $result));
    }

    public function answerView($id) {
        //here we are loading up the view so the user can answer a question
        //as always do a check to see if the user is logged in
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            redirect('/auth');
        }
        //check if the question is closed, if its not let it be answered, if it is closed just reload the question
        $isClosed = $this->searchModel->isClosed($id);
        if ($isClosed === 0) {
            $id = $this->uri->segment(3, 0);
            $data = array('id' => $id);
            $this->load->view('answerView', $data);
        } else {
            redirect('/mainController/questions/' . $id);
        }
    }

    public function answerQuestion() {
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            redirect('/auth');
        }
        //get the values entered in the answer and calls the model with the information
        $id = $this->input->post('id');
        $text = $this->input->post('text');
        $this->answerModel->answerQ($id, $text);
        redirect('/mainController/questions/' . $id);
    }

    public function editView($id) {
        //this function loads the edit view
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            redirect('/auth');
        }
        //again logged in and is closed if not load the view up with the title and text in the textbox
        $isClosed = $this->searchModel->isClosed($id);
        if ($isClosed === 0) {
            $id = $this->uri->segment(3, 0);
            $data = $this->editModel->loadEdit($id);
            $result = $data->result();
            $this->load->view('editView', array('id' => $id, 'result' => $result));
        } else {
            redirect('/mainController/questions/' . $id);
        }
    }

    public function editQuestion() {
        //this function submits the edit
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            redirect('/auth');
        }
        //get the values from the form and load the question up
        $id = $this->input->post('id');
        $text = $this->input->post('text');
        $title = $this->input->post('title');
        $this->editModel->edit($id, $text, $title);
        redirect('/mainController/questions/' . $id);
    }

    public function closeQuestion($id) {
        //admins and mods can close the question
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            redirect('/mainController');
        }
        //gets the users role and then closes the question in the model and redirects the user to the main page
        $role = $this->adminModel->getRole($username);
        if ($role > 1) {
            $this->adminModel->close($id);
        }
        redirect('/mainController');
    }

    public function adminPage() {
        //specific page for admins and mods
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            redirect('/auth');
        }
        $role = $this->adminModel->getRole($username);
        //load page if they have the role else redirect to homepage
        if ($role > 1) {
            $this->load->view('adminView', array('role' => $role));
        } else {
            redirect('/mainController');
        }
    }

    public function banUser() {
        //ban user method
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            redirect('/auth');
        }
        //gets the entered username and then calls the model to ban them
        $role = $this->adminModel->getRole($username);
        $uName = $this->input->post('username');
        if ($role > 1) {
            $this->adminModel->ban($uName);
        }
        redirect('/mainController');
    }

    public function appointMod() {
        //allows admin to appoint mods
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            redirect('/auth');
        }
        $role = $this->adminModel->getRole($username);
        $uName = $this->input->post('modUser');
        //gets entered username and makes sure the user is an admin and then calls the model to make them admin
        if ($role == 3) {
            $this->adminModel->addMod($uName);
        }
        redirect('/mainController');
    }

    public function voteUp($id) {
        //allows logged in users to up vote a question
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            print "<script type=\"text/javascript\">alert('Must log in to vote');</script>";
            redirect('/auth');
        }
        $this->searchModel->voteUp($id);
        redirect('/mainController/questions/' . $id);
    }

    public function voteDown($id) {
        //allows logged in users to vote down a question
        $username = $this->authlib->is_loggedin();
        if ($username === false) {
            //print "<script type=\"text/javascript\">alert('Must log in to vote');</script>";
            redirect('/auth');
            //$this->load->view('/login_view');
        }
        $this->searchModel->voteDown($id);
        redirect('/mainController/questions/' . $id);
    }

    public function userProfile($id) {
        //Function which loads the user profile page
        $rep = $this->user->getRep($id);
        $role = $this->user->getRole($id);
        $this->load->view('profileView', array('id' => $id, 'rep' => $rep, 'role' => $role));
    }

    public function userSearch() {
        $user = $this->input->get('user');
        $userTrimmed = trim($user);
        $data = $this->searchModel->userSearch($userTrimmed);
        $this->load->view('searchView', array('user' => $data));
    }

    public function advancedSearch() {
        $tags = $this->input->get('tags');
        $title = $this->input->get('title');
        $user = $this->input->get('user');
        //used to search, we get all input data
        //trim and explode the data by space
        $arr = array_map('trim', explode(" ", $title));
        $titleArray = array();
        foreach ($arr as $row) {
            if ($row !== "") {
                $titleArray[] = array('title' => $row);//make sure theres no white space
            }
        }
        //same again for tags
        $arrTags = array_map('trim', explode(" ", $tags));
        $tagArray = array();
        foreach ($arrTags as $row) {
            if ($row !== "") {
                $tagArray[] = $row;
            }
        }
        //send data to model
        $data = $this->searchModel->advancedSearch($titleArray, $tagArray, $user);
        //return the model data to the view
        $this->load->view('searchView', array('advSearch' => $data));
    }
    
    public function sort(){
        $tags = $this->input->get('tags');
        print_r($tags);
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

