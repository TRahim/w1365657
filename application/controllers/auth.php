<?php

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('authlib');
        $this->load->helper('url');
    }

    public function index() {
        $loggedin = $this->authlib->is_loggedin();
        $this->load->view('login_view', array('user' => $loggedin));
        //redirect('/auth/login'); // url helper function
    }

    public function register() {
        $this->load->view('register_view', array('errmsg' => ''));
    }

    public function createaccount() {
        $name = $this->input->post('name');
        $username = $this->input->post('uname');
        $password = $this->input->post('pword');
        $conf_password = $this->input->post('conf_pword');

        if (!($errmsg = $this->authlib->register($name, $username, $password, $conf_password))) {
            redirect('/auth/login');
        } else {
            $data['errmsg'] = $errmsg;
            $this->load->view('login_view', $data);
        }
    }

    public function login() {
        $data['errmsg'] = '';
        $this->load->view('login_view', $data);
    }

    public function logout() {
//Destroy the session and display the login page.
        $this->session->sess_destroy();
        redirect('/mainController');
    }

    public function show_change_password() {
        $this->load->view('change_password_view');
    }

    public function get_password_details() {
        $username = $this->input->post('username');
        $oldpassword = $this->input->post('old_password');
        $newpassword = $this->input->post('new_password');
        $confirmpassword = $this->input->post('confirm_password');
        $data = array('username' => $username, 'oldpassword' => $oldpassword, 'newpassword' => $newpassword, 'confirmpassword' => $confirmpassword);
        $this->authlib->change_password($data);
        $this->show_change_password();
    }

    public function authenticate() {
        $username = $this->input->post('uname');
        $password = $this->input->post('pword');
        $user = $this->authlib->login($username, $password);
        //$loggedin = $this->authlib->is_loggedin();
        //print_r($loggedin);
        
        if ($user !== false) {
            //$this->load->view('homepage', $loggedin); //array('name' => $user['username']));//, 'loggedin' => $loggedin));
            redirect("/mainController");
        } else {
            $data['errmsg'] = 'Unable to login - please try again';
            $this->load->view('login_view', $data);
        }
    }

}
