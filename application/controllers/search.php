<?php

class search extends CI_Controller {
    //The search here works the same way as the search in the mainController advanced search function
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('simonModel');
    }

    public function index() {
        $this->load->view('simonSearchView');
    }

    public function questions() {
        $question = $this->input->get('words');
        $tags = $this->input->get('tags');
        $user = $this->input->get('user');
        $arr = array_map('trim', explode(" ", $question));
        $array = array();
        foreach ($arr as $row) {
            if ($row !== "") {
                $array[] = array('title' => $row);
            }
        }
        $arrTags = array_map('trim', explode(" ", $tags));
        $tagArray = array();
        foreach ($arrTags as $row) {
            if ($row !== "") {
                $tagArray[] = $row;
            }
        }
        $data = $this->simonModel->advancedSearch($array,$tagArray,$user);
        //$tagData = $this->simonModel->getTags($data);
        //$userData = $this->simonModel->getUser($data);
        //$questionData = $this->simonModel->getQuestion($data);
        $json = json_encode($data);
        $this->load->view('simonQuestionView', array('json' => $json));
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

